//
//  LinkedList.swift
//  LinkedList data structure file
//
//  Created by Mychel Antonacio on 29/02/2020.
//  Copyleft © 2020 Mychel Antonacio. MIT License.
//


import Foundation


class Node<T>{
    
    var next: Node<T>?
    weak var previous: Node<T>?
    var data: T //next step create generic type...
    
    init(data: T) {
        self.data = data
    }
}


extension Node: CustomStringConvertible {
    public var description: String {
        return "[\(data)]"
  }
}

class LinkedList<T>{
    
    private var headNode: Node<T>?
    private var tailNode: Node<T>?
    
    
    public var first: Node<T>?{
        return headNode
    }
    
    public var last: Node<T>?{
        return tailNode
    }
    
    public var lenght:Int{
        var count = 1
        
        if first == nil{
            return 0
        }else{
            var node = first
            while node != nil && node!.next != nil  {
                node = node!.next
                count += 1
            }
        }
        //print("lenght - count = \(count)")
        return count
    }
    
    public func add(newValue: T){
        
        let newNode = Node(data: newValue)
        
        if let lastNode = last {
            //print("(if)add - newValue = \(newValue)")
            newNode.previous = lastNode
            lastNode.next = newNode
        } else{
            //print("(else)add - newValue = \(newValue)")
            headNode = newNode
        }
        tailNode = newNode
    }
    
    public func nodeAt(index: Int) -> Node<T>? {
      
      if index >= 0 {
        var node = first
        var idx = index
        
        while node != nil {
          if idx == 0 { return node }
          idx -= 1
          node = node!.next
        }
      }
      return nil
    }

    public func remove(node: Node<T>) -> T {
        
      let prev = node.previous
      let next = node.next

      if let prev = prev {
        prev.next = next
      } else {
        headNode = next
      }
        
      next?.previous = prev

      if next == nil {
        tailNode = prev
      }

      node.previous = nil
      node.next = nil

      return node.data
    }
}//end_class...


extension LinkedList: CustomStringConvertible {
    
  public var description: String {
    var text = "H-["
    var node = first
    
    while node != nil {
      text += "\(node!.data)"
      node = node!.next
      if node != nil { text += ", " }
    }
    return text + "]-T"
  }
}



//TEST
var linkedList = LinkedList<Int>()

print(linkedList.lenght)

linkedList.add(newValue: 10)
print(linkedList.lenght)

linkedList.add(newValue: 20)
print(linkedList.lenght)

linkedList.add(newValue: 30)
print(linkedList.lenght)

let nodeAux = linkedList.nodeAt(index: 0)
print(nodeAux?.data)

print(linkedList)

if let nodeAux2 = nodeAux {
    print(linkedList.remove(node: nodeAux2))
}

print(linkedList)




var linkedList2 = LinkedList<String>()

print(linkedList2.lenght)

linkedList2.add(newValue: "40")
print(linkedList2.lenght)

linkedList2.add(newValue: "50")
print(linkedList2.lenght)

linkedList2.add(newValue: "60")
print(linkedList2.lenght)

let nodeAux2 = linkedList2.nodeAt(index: 0)
print(nodeAux2?.data)

print(linkedList2)

if let nodeAux3 = nodeAux2 {
    print(linkedList2.remove(node: nodeAux3))
}

print(linkedList2)



//STACK
class Stack<T>{
    
    var linkedListAux = LinkedList<T>()
    
    
    var size: Int{
        return linkedListAux.lenght
    }
    
    var peek: T?{

        if let value = linkedListAux.last{
            return value.data
        }
        else{
            return nil
        }
    }
    
    public func push(value: T){
        linkedListAux.add(newValue: value)
    }
    
    public func pop() -> T?{
        guard let nodeToRemove = linkedListAux.last else{
            return nil
        }
        return linkedListAux.remove(node: nodeToRemove)
    }
}

extension Stack: CustomStringConvertible {

  public var description: String {

    var count = 0
    var text = "["

    while count < size{
        text += "\(pop()!)"
        if (pop() != nil) { text += ", " }
        count += 1
    }
    print("\(text)]")
    return text + "]"
  }
}


print("Stack test \n")
var stackTest = Stack<Int>()

print(stackTest)

stackTest.push(value: 10)



